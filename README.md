# Openvas scanner for samma.io
Brings up the full OpenVas scanner stack


- gsad
- gvmd
- gvmd-postgress
- openvas (scanner)
- openvas-exporter


### Access 

you can then accesss the web interface to work with the scanner and create scans and reports.



## Testing

### Deps

Install docker and docker-compose

### Run 

The scanners spinns up the complete stack of openvas.
Include the demon and postgres SQL server.

This is a easy stepp to bring up the full openvas for test.

```
docker-compose up
```


And go to 


http://localhost:4000 and login with admin/admin

## Productions

For productions its better to use our Helm Chart and deploy into a Kubernetes cluster.

## ELK Ready


The openvas-exporter takes the reports and print them out as JSON. Then a Logstash reads the file and spits them out inte stout.
You cna easy configure Logstash to send the logs to Elasticsearch ore other toold.


Spin up the Samma Stack to have working Elasticsearch stack running.
Then you should see your openvas result comes up in elasticsearch.

## Helm chart to use with kubernetes are in the samma helm repo

## Build

To build and expand the stack use the docker-compose_build.yaml. Then the images are build and started.

```
docker-compose -f docker-compose_build.yaml up
```


#### Happy scanning 